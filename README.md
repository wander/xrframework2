## XRFramework2 (HDRP)

This framework adds 'pretty' XR interaction capabilities with Network support.
The network only contains the XR part, that is for exammple interaction with UI in XR.
The underlaying networking, eg. discovery of other servers or movement implementations are put in the networking package.


### Dependencies (GIT Pacakages)
- networking
- utils

### Usage

Ensure the following settings are set, OpenXR on.

![Image](./xrsettings.png)

And 
![Image](./xrsettings2.png)


### Sample

![Image](./sample.png)