//#if WANDER_VR

using UnityEngine;
using UnityEngine.InputSystem;

namespace Wander
{
	/* As The XR Interaction Toolkit is not compatible with Oculus integration, this scripts help fixes the animations on the buttons. */
    public class OculusControllerHelper : MonoBehaviour
    {
		public InputActionProperty button1;
		public InputActionProperty button2;
		public InputActionProperty button3;
		public InputActionProperty trigger;
		public InputActionProperty grip;
		public InputActionProperty joyX;
		public InputActionProperty joyY;

		public Animator m_animator;

        private void Awake()
        {
        //      m_animator = GetComponent<Animator>();
        }

        void Update()
		{
			if (m_animator != null)
			{
				m_animator.SetFloat( "Button 1", (button1.action!=null && button1.action.IsPressed()) ? 1.0f : 0.0f );
				m_animator.SetFloat( "Button 2", (button2.action!=null && button2.action.IsPressed()) ? 1.0f : 0.0f );
				m_animator.SetFloat( "Button 3", (button3.action!=null && button3.action.IsPressed()) ? 1.0f : 0.0f );

				m_animator.SetFloat( "Joy X", joyX.action!=null ? joyX.action.ReadValue<Vector2>().x : 0 );
				m_animator.SetFloat( "Joy Y", joyY.action!=null ? joyY.action.ReadValue<Vector2>().y : 0 );

				m_animator.SetFloat( "Trigger", trigger.action!=null ? trigger.action.ReadValue<float>() : 0);
				m_animator.SetFloat( "Grip", grip.action!=null ? grip.action.ReadValue<float>() : 0);
			}
		}
	}
}

//#endif