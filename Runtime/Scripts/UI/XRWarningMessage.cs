//#if WANDER_VR

using TMPro;
using UnityEngine;

namespace Wander
{
    public class XRWarningMessage: MonoBehaviour
    {
        public float popupDuration = 3;
        public float fadeTime = 2;

        Color initialColor;
        float curDuration;
        float curFadeTime;

        private void Start()
        {
            initialColor = GetComponent<MeshRenderer>().sharedMaterial.GetColor( "_Color" );
        }

        void Update()
        {
            curDuration += Time.deltaTime;
            if ( curDuration > popupDuration )
            {
                curFadeTime += Time.deltaTime;
                float a = curFadeTime / fadeTime;
                if (a >= 1)
                {
                    gameObject.SetActive( false );
                }
                else
                {
                    float newAlpha = initialColor.a * (1-a);
                    Color c = initialColor;
                    c.a = newAlpha;
                    GetComponent<MeshRenderer>().sharedMaterial.SetColor( "_Color", c );
                }
            }
        }

        public void AddWarning(string msg)
        {
            curDuration = 0;
            curFadeTime = 0;
            transform.GetChild(0).GetComponent<TextMeshPro>().text = msg;
            gameObject.SetActive( true );
        }
    }
}

//#endif