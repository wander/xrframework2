using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.UI;

#pragma warning disable CS0618

namespace Wander
{
    public class UIRayEventDispatcher : MonoBehaviour, IUIInteractor
    {
        public float distance = 30;
        public Material validBeam;
        public Material invalidBeam;

        LineRenderer lineRenderer;
        ActionBasedController controller;
        XRUIInputModule m_InputModule;
        XRUIInputModule m_RegisteredInputModule;

        // Returns whether ray is on UI element.
        bool isOnUI;
        public bool IsOnUI
        {
            private set
            {
                {
                    isOnUI=value;
                    XRUIState.RayIsOnUI=value;
                }
            }
            get { return isOnUI;  }
        }

        // Distance from controller to UI element. Is set to 'distance' (public field) if not 'IsOnUI'.
        float currentHitDistance;
        public float CurrentHitDistance
        {
            private set { currentHitDistance=value;}
            get { return currentHitDistance;}
        }

        private void Awake()
        {
            controller   = GetComponent<ActionBasedController>();
            lineRenderer = GetComponent<LineRenderer>();
        }

        public void Start()
        {
            RegisterWithXRUIInputModule();
        }

        void RegisterWithXRUIInputModule()
        {
            if (m_InputModule == null)
                FindOrCreateXRUIInputModule();

            if (m_RegisteredInputModule == m_InputModule)
                return;

            UnregisterFromXRUIInputModule();

            m_InputModule.RegisterInteractor( this );
            m_RegisteredInputModule = m_InputModule;
        }

        void UnregisterFromXRUIInputModule()
        {
            if (m_RegisteredInputModule != null)
                m_RegisteredInputModule.UnregisterInteractor( this );

            m_RegisteredInputModule = null;
        }

        void FindOrCreateXRUIInputModule()
        {
            var eventSystem = FindObjectOfType<EventSystem>();
            if (eventSystem == null)
                eventSystem = new GameObject( "EventSystem", typeof( EventSystem ) ).GetComponent<EventSystem>();
            else
            {
                // Remove the Standalone Input Module if already implemented, since it will block the XRUIInputModule
                var standaloneInputModule = eventSystem.GetComponent<StandaloneInputModule>();
                if (standaloneInputModule != null)
                    Destroy( standaloneInputModule );
            }

            m_InputModule = eventSystem.GetComponent<XRUIInputModule>();
            if (m_InputModule == null)
            {
                m_InputModule = eventSystem.gameObject.AddComponent<XRUIInputModule>();
            }
        }

        public bool TryGetUIModel( out TrackedDeviceModel model )
        {
            if (m_InputModule != null)
            {
                return m_InputModule.GetTrackedDeviceModel( this, out model );
            }

            model = new TrackedDeviceModel( -1 );// TrackedDeviceModel.invalid;
            return false;
        }

        public void UpdateUIModel( ref TrackedDeviceModel model )
        {
            if (model.currentRaycast.isValid)
            {
                // model.currentRaycast.distance is not correct, compute ourselves
                currentHitDistance = (model.currentRaycast.worldPosition-transform.position).magnitude;
                isOnUI = true;
            }
            else 
            {
                currentHitDistance = distance;
                isOnUI = false;
            }

            bool clickedIt = controller.activateInteractionState.active; // This is the trigger.

            model.position = transform.position;
            model.orientation = transform.rotation;
            model.select = clickedIt;
            model.raycastLayerMask = LayerMask.GetMask( "UI" );

            var raycastPoints = model.raycastPoints;
            raycastPoints.Clear();

            raycastPoints.Add( transform.position + transform.forward*lineRenderer.GetPosition(0).z /*Offset from hand/controller*/ );
            raycastPoints.Add( transform.position + transform.forward*distance );
        }

        private void LateUpdate()
        {
            if (!XRUIState.XRAnyMenuOn)
                return;

            if ( IsOnUI )
            {
                lineRenderer.sharedMaterial = validBeam;
                lineRenderer.SetPosition( 1, new Vector3( 0, 0, CurrentHitDistance ) );
                //Debug.Log( "dist = " + CurrentHitDistance );
            }
            else
            {
                lineRenderer.sharedMaterial = invalidBeam;
                lineRenderer.SetPosition( 1, new Vector3( 0, 0, distance ) );
                //Debug.Log( "dist = " + distance );
            }
        }

    }
}


#pragma warning restore CS0618