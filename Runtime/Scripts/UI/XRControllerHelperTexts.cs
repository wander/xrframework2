//#if WANDER_VR

using TMPro;
using UnityEngine;

namespace Wander
{
    [System.Serializable]
    public struct HelperTexts
    {
        public string Trigger;
        public string Grip;
        public string Button1;
        public string Button2;
        public string Joy;
        public string BigText;
        public string BigTextDescription;
    }

    public class XRControllerHelperTexts : MonoBehaviour
    {
        public GameObject button1Parent;
        public GameObject button2Parent;
        public GameObject triggerParent;
        public GameObject gripParent;
        public GameObject joypadParent;
        public GameObject bigTextParent;

        public TextMeshPro button1Text;
        public TextMeshPro button2Text;
        public TextMeshPro triggerText;
        public TextMeshPro gripText;
        public TextMeshPro joyText;
        public TextMeshPro bigText;
        public TextMeshPro bigTextDescription;

        private void Start()
        {
        }

        public void SetHelperTexts( HelperTexts texts)
        {
            button1Text.text = texts.Button1;
            button2Text.text = texts.Button2;
            triggerText.text = texts.Trigger;
            gripText.text = texts.Grip;
            joyText.text = texts.Joy;
            bigText.text = texts.BigText;
            bigTextDescription.text = texts.BigTextDescription;
            // ------------
            button1Parent.SetActive( !string.IsNullOrWhiteSpace( texts.Button1 ) );
            button2Parent.SetActive( !string.IsNullOrWhiteSpace( texts.Button2 ) );
            triggerParent.SetActive( !string.IsNullOrWhiteSpace( texts.Trigger ) );
            gripParent.SetActive( !string.IsNullOrWhiteSpace( texts.Grip ) );
            joypadParent.SetActive( !string.IsNullOrWhiteSpace( texts.Joy ) );
            bigTextParent.SetActive( !string.IsNullOrWhiteSpace( texts.BigText ) );
        }
    }
}

//#endif