using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode.Transports.UTP;

namespace Wander
{
    [RequireComponent(typeof(Canvas))]
    public class XRCreateOrJoin : MonoBehaviour
    {
        [Header("References")]
        public Camera hmdCamera;
        public GameObject xrNetworkManager;
        public Button createButton;
        public GameObject joinButtonsParent;
        //public Button uiCloseButton;

        [Header("Menu position")]
        public float vrOffsetZ = 4;
        public float vrOffsetY = 0;
        public float angleOffset = 0;

        [Header("Debug")]
        public bool autoStartAsHost;

        UnityTransport transport;
        NetworkDiscovery discovery;
        NetworkManager netManager;

        private void Awake()
        {
            // Try auto resolve references if not set.
            if (hmdCamera == null)
            {
                hmdCamera = GetComponent<Canvas>().worldCamera;
            }
            if (xrNetworkManager == null)
            {
                xrNetworkManager = GameObject.Find( "XRNetworkManager" );
            }

        //    Debug.Assert( hmdCamera != null, "hmdCamera not set" );
            Debug.Assert( xrNetworkManager != null, "XRNewtorkManager not set" );
            Debug.Assert( createButton != null, "createButton is null reference" );

            transport  = xrNetworkManager.GetComponent<UnityTransport>();
            discovery  = xrNetworkManager.GetComponent<NetworkDiscovery>();
            netManager = xrNetworkManager.GetComponent<NetworkManager>();
            createButton.onClick.AddListener( OnCreate );
            
            for ( int i = 0; i<joinButtonsParent.transform.childCount; i++)
            {
                Button button = joinButtonsParent.transform.GetChild( i ).GetComponent<Button>();
                button.onClick.AddListener( () =>
                {
                    /// Button contains exact name of server
                    OnJoin(button.name);
                } );
                button.gameObject.SetActive( false ); /// hide it
            }
        }

        void OnCreate()
        {
            discovery.StartBroadcasting();
            netManager.StartHost();
            gameObject.SetActive( false ); /// Hide this menu, started as host.
            XRUIState.CreateOrJoinMenuOn = false;
            Debug.Log( "Stopped received and started broadcasting as: " + discovery.serverName );
        }

        void OnJoin(string serverName)
        {
            List<ServerEntry> entries = discovery.GetServerEntries();
            var idx = entries.FindIndex( entry => entry.name == serverName );
            if (idx >= 0)
            {
                transport.ConnectionData.Address = entries[idx].ip;
                transport.ConnectionData.Port = entries[idx].port;
                if (netManager.StartClient())
                {
                    discovery.StopDiscovery(); /// No longer need to discover.
                    gameObject.SetActive( false );
                    XRUIState.CreateOrJoinMenuOn = false;
                    Debug.Log( "Stopped network discovery" );
                }
            }
        }

        private void Start()
        {
            XRUIState.CreateOrJoinMenuOn = true;

            /// Generate 'unique server name'
            discovery.serverName = "LIDAR-" + UnityEngine.Random.Range( 10000, 99999 );

            if ( autoStartAsHost )
            {
                OnCreate();
                return;
            }

            createButton.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = "Create room\n" + discovery.serverName;

            /// Dont' know if we are going to broadcast or receive, so start receiving.
            discovery.StartReceiving();
            StartCoroutine( PopulateJoinButtons() );

            ///RepositionVR();
        }

        IEnumerator PopulateJoinButtons()
        {
            while (true)
            {
                List<ServerEntry> entries = discovery.GetServerEntries();

                /// There is only a fixed number of join buttons (no scroll view, so complicated haha).
                int numEntries = Mathf.Min(entries.Count, joinButtonsParent.transform.childCount); 

                /// First disable all buttons.
                for ( int i = 0; i < joinButtonsParent.transform.childCount; i++ )
                {
                    joinButtonsParent.transform.GetChild( i ).gameObject.SetActive( false );
                }

                /// Populate join buttons.
                for ( int i = 0; i < numEntries; i++ )
                {
                    Button button = joinButtonsParent.transform.GetChild( i ).GetComponent<Button>();
                    var textMesh  = button.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>();
                    textMesh.text = "Join " + entries[i].name;
                    button.name = entries[i].name;
                    button.gameObject.SetActive( true );
                    Debug.Log( "Server available: " + entries[i].name );
                }

                yield return new WaitForSeconds( 1 );
            }
        }

        void RepositionVR()
        {
            if (hmdCamera==null)
            {
                Debug.LogWarning( "Cannot reposition VR, no hdm camera assigned." );
                return;
            }
            var cam = hmdCamera;
            var forward =  cam.transform.forward;
            forward.y = 0;
            forward.Normalize();
            forward = Quaternion.Euler( 0, angleOffset, 0 ) * forward;
            transform.position = cam.transform.position +  forward * vrOffsetZ + Vector3.up * vrOffsetY;
            transform.rotation = Quaternion.Euler( 0, cam.transform.eulerAngles.y+angleOffset, 0 );
        }


        /// <summary>
        /// ---- These functions are invoked from clicking on a menu item in the XR Network Menu -----
        /// </summary>
        /// <param name="value"></param>
        public void SetGlobalLODBias( float value )
        {

        }

        public void SetViewType( int value )
        {

        }

        public void SetQualityLevel( float value )
        {
        }

        public void EnableDisablePostProcessing( bool enable )
        {
        }

    }
}

//#endif