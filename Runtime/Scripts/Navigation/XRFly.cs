using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


#pragma warning disable CS0618

namespace Wander
{
    [RequireComponent( typeof( ActionBasedController ) )]
    public class XRFly : MonoBehaviour
    {
        public HelperTexts helperTexts;
        public float moveAccel  = 0.2f;
        public float brakeSpeed = 0.5f;
        public bool  rotateOnThumbstick = true;
        public float rotateAmount = 45;
        public float lineLength = 20;
        public Material lineMaterial;

        Vector3 velocity;
        ActionBasedController controller;
        XROrigin origin;
        LineRenderer lineRenderer;

        private void Awake()
        {
            origin = FindObjectOfType<XROrigin>();
            controller = GetComponent<ActionBasedController>();
            lineRenderer = GetComponent<LineRenderer>();
        }

        private void OnEnable()
        {
            GetComponent<XRControllerHelperTexts>().SetHelperTexts( helperTexts );
            lineRenderer.enabled = false;
            lineRenderer.sharedMaterial = lineMaterial;
            lineRenderer.SetPosition( 1, new Vector3( 0, 0, lineLength ) );
            XRNetworkState.LaserLength[(int)XRHand.Right] = lineLength;
            XRNetworkState.LaserColor[(int)XRHand.Right]  = lineMaterial.GetColor( "_Color" );
        }

        private void OnDisable()
        {
            GetComponent<LineRenderer>().enabled = true;
        }

        void Update()
        {
            if (XRUIState.SettingsMenuOn)
                return;

            var joyInputY = controller.translateAnchorAction.action.ReadValue<Vector2>();
            var joyInputX = controller.rotateAnchorAction.action.ReadValue<Vector2>();

            Vector3 accel = new Vector3( rotateOnThumbstick ? 0 : joyInputX.x * moveAccel, 0, joyInputY.y * moveAccel);
            accel = transform.TransformVector( accel );
            velocity += accel * Time.deltaTime;

            float brake = MathUtil.Friction( brakeSpeed, Time.deltaTime );
            velocity *= brake;
            
            if (rotateOnThumbstick)
            {
                if (controller.rotateAnchorAction.action.WasPressedThisFrame())
                {
                    if (joyInputX.x < -0.5f) origin.transform.Rotate( 0, -45, 0, Space.World );
                    if (joyInputX.x > 0.5f) origin.transform.Rotate( 0, 45, 0, Space.World );
                }
            }

            origin.transform.position += velocity * Time.deltaTime;
        }
    }
}

#pragma warning restore CS0618 // Type or member is obsolete