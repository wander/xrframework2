using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

#pragma warning disable CS0618

namespace Wander
{
    [RequireComponent(typeof(ActionBasedController))]
    public class XRChangeTool : MonoBehaviour
    {
        [System.Serializable]
        public struct Mode
        {
            public List<MonoBehaviour> enableScripts;
        }

        public HelperTexts emptyTexts;
        public bool disableRayOnNoTool = true;
        public bool setFirstToolImmediately = true;
        public InputActionProperty activateKey;
        public List<Mode> tools = new List<Mode>();
        public KeyCode debugCode = KeyCode.Alpha1;

        public int CurrentMode => modeIndex;

        int modeIndex;
        LineRenderer lineRenderer;

        private void Awake()
        {
            lineRenderer = GetComponent<LineRenderer>();

            // If main menu is turned on, disable this script.
            XRUIState.OnSettingsEnabled += () =>
            {
                enabled = false;
                DisableOldMode();
            };
            // If main menu turned off, re enable current tool/mode.
            XRUIState.OnSettingsDisabled += () =>
            {
                enabled = true;
                SetMode( modeIndex ); // Renable current mode.
            };
        }

        void Start()
        {
            lineRenderer.positionCount = 2;
            lineRenderer.useWorldSpace = false;
            lineRenderer.enabled = false;
            GetComponent<XRControllerHelperTexts>().SetHelperTexts( emptyTexts );
            if (setFirstToolImmediately)
            {
                SetMode( 0 );
            }
        }

        void DisableOldMode()
        {
            Mode m = tools[modeIndex];
            if (m.enableScripts!= null)
            {
                m.enableScripts.ForEach( s =>
                {
                    if ( s != null )
                        s.enabled = false;
                } );
            }
        }

        public void SetMode( int mode )
        {
            bool didEnabelScripts = false;

            if (tools.Count > 0)
            {
                // Disable old.
                DisableOldMode();

                // now set to new 
                modeIndex = mode;

                var m = tools[modeIndex];
                if (m.enableScripts!= null)
                {
                    m.enableScripts.ForEach( s =>
                    {
                        if (s!=null)
                        {
                            didEnabelScripts = true;
                            s.enabled = true;
                        }
                    } );

                    // Tools are themselves responsible for setting the correct laser length.
                    // It is auto set to 0, if nothing is specified.
                    XRNetworkState.LaserLength[(int)XRHand.Left] = 0;
                }
            }

            if (!didEnabelScripts)
            {
                GetComponent<XRControllerHelperTexts>().SetHelperTexts( emptyTexts );
                if (disableRayOnNoTool)
                {
                    GetComponent<LineRenderer>().enabled = false;
                }
            }
            else
            {
                GetComponent<LineRenderer>().enabled = true;
            }
        }

        public void IncrementMode()
        {
            int newMode = modeIndex+1;
            if (newMode == tools.Count)
                newMode = 0;
            SetMode( newMode );
        }

        public void DecrementMode()
        {
            int newMode = modeIndex-1;
            if (newMode < 0) newMode = tools.Count-1;
            if (newMode < 0) newMode = 0; // If no tools are added.
            SetMode( newMode );
        }

        private void Update()
        {
            if (Input.GetKeyDown( debugCode )) // for debugging on keyb
                IncrementMode(); 

            if (activateKey.action != null && activateKey.action.WasPressedThisFrame())
            {
                IncrementMode();
            }
        }
    }
}


#pragma warning restore CS0618